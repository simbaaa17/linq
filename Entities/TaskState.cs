﻿using System;
using System.Collections.Generic;
using System.Text;

namespace linq.Entities
{
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished, 
        Canceled
    }
}
