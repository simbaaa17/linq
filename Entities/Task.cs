﻿using System;
using System.Collections.Generic;
using System.Text;

namespace linq.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState TaskState { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }

        public override string ToString() => $"{Id} - {Name}";
    }
}
