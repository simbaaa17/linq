﻿using System;
using System.Collections.Generic;
using System.Text;

namespace linq.Entities
{
    public class TaskStateModel
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public override string ToString() => $"{Id} - {Value}";
    }
}
